package lab;

/**
 * Aufgabe H1
 * 
 * Abgabe von: Magnus Dierking 2693156, Jonathan Lippert 2501536 und Tilman Froelich 2342436
 */

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.PriorityQueue;

import frame.Graph;
import frame.SSSPNode;

public class Algorithms {

	/**
	 * 
	 * @param graph     input graph
	 * @param edgeRatio specifying until what ratio of actualEdge#/possibleEdge# the
	 *                  Johnsons is to run, otherwise FloydWarshall will be run
	 * @return a hashmap with the nodeId´s as entries for "their" SSSPNodes
	 */

		//		  source	 disstance  node of target
	public static Hashtable<Integer, Hashtable<Integer, SSSPNode>> HybridSSSP(Graph graph, double edgeRatio) { //ruft entweder Johnson oder FloydWarshall auf
		int thisEdgeRatio = (graph.getNodeNumber() * (graph.getEdgeNumber() -1)) / graph.getEdgeNumber(); //EdgeRatio dieses Graphen bestimmen
		if (thisEdgeRatio <= edgeRatio) { //johnson fuer sparse graphs
			return Johnson(graph);
		}
		else {
			return FloydWarshall(graph); //dense graphs
			}
		}

	/**
	 * 
	 * @param graph on which to run the algorithm
	 * @return a hashmap with the nodeId´s as entries for "their" SSSPNodes; null
	 *         if negative cycle exists
	 */
	public static Hashtable<Integer, Hashtable<Integer, SSSPNode>> Johnson(Graph graph) {
		int nodeNumb = graph.getNodeNumber(); 
		// neuen Graphen mit s 
		Hashtable<Integer,Hashtable<Integer, Integer>> edges_j = new Hashtable<>(); //alle Edges finden
		for(int from = 0; from< nodeNumb; from++) { //alle Knoten durchgehen und verbindungen pruefen
			//System.out.println(from);
			edges_j.put(from,new Hashtable<>()); //fuer jeden Knoten eine Hashtable
			for(int to : graph.getNeighbours(from)) { //und mit Nachbarn fuellen
				//System.out.println("From: " + from + " To: " + to + " w: " +graph.getEdge(from, to)  );
				edges_j.get(from).put(to,graph.getEdge(from, to));
			}
		} //extra Knoten hinzufuegen und mit allen Knoten verbinden
		int s = graph.getNodeNumber();
		edges_j.put(s,new Hashtable<>());
		for(int to = 0; to < nodeNumb; to++) {
			edges_j.get(s).put(to,0);
		} //daraus neuen Graph erstellen
		Graph graph_j = new Graph(nodeNumb+1,edges_j);
		//BellmanFord laufen lassen
		Hashtable<Integer, SSSPNode> BF_result = BellmanFord(graph_j,s);
		if(BF_result == null ) return null; // es gibt negative zyklen 
		//Kannten neugewichten und s rausschmei�en		
		edges_j = new Hashtable<>();
		for(int from = 0; from< nodeNumb; from++) { //alle Knoten durchgehen
			edges_j.put(from,new Hashtable<>());
			for(int to : graph.getNeighbours(from)) { //alle Neibours durchgehen
				//Johnson Formel: wie funktioniert die?
				edges_j.get(from).put(to,graph.getEdge(from, to)+ BF_result.get(from).distance - BF_result.get(to).distance); //in Hashtable bei "from" setzen: "to" + distanz
			}
		}
		graph_j = new Graph(nodeNumb, edges_j); //neuen Graphen aus den edges ohne s erstellen
		//Dijkstra auf neuen Graphen anweden
		Hashtable<Integer,Hashtable<Integer,SSSPNode>> D_result = new Hashtable<>();
		for(int i = 0; i< nodeNumb; i++) { //viele Dijkstra von jedem knoten aus laufen lassen
			D_result.put(i, Dijkstra(graph_j, i)); 
		}
		
		Hashtable<Integer,Hashtable<Integer,SSSPNode>> end_result = (Hashtable<Integer, Hashtable<Integer, SSSPNode>>) D_result.clone(); //new Hashtable<>();
		for(int source = 0; source < nodeNumb; source++) {
			for( int target=0; target < nodeNumb; target++) { //alle Verbinungen zwischen allen Knoten definieren
				if(end_result.get(source).get(target).distance == null) { //keine Verbindung
					continue;
				}else {
					//Johnson Formel: wie funktioniert die?
					end_result.get(source).get(target).distance = 
							D_result.get(source).get(target).distance + BF_result.get(target).distance - BF_result.get(source).distance;
				}
			}

		}
		return D_result;
	}

	/**
	 * 
	 * @param graph  the input graph
	 * @param source a valid Node ID of the graph
	 * @return for each node in the graph an entry in the hashmap which leads to a
	 *         datatype storing the results of the algorithm (as on the slides);
	 *         returns null if negative cycle exists
	 */
	public static Hashtable<Integer, SSSPNode> BellmanFord(Graph graph, int source) {
		//initialisieren als Hashtable
		Hashtable<Integer, SSSPNode> result = init(graph, source);
		
		for (int i=1; i<graph.getNodeNumber(); i++) { //oft genug alle Kanten relaxen, durch 3x for Schleife, ist am ende alles relaxed
			for (int id=0; id<graph.getNodeNumber(); id++) { //fuer alle knoten
				for (int nb:graph.getNeighbours(id)) { //alle neibours nb durchgenen, damit alle kanten relaxet werden
					result = relax(result, id, nb, graph.getEdge(id, nb));					
				}
			}
		}
		//negative loops
		for (int id=0; id<graph.getNodeNumber(); id++) { //fuer alle knoten
			for (int nb:graph.getNeighbours(id)) { //alle neibours nb durchgenen, damit alle kanten relaxet werden
				//assert (result.get(nb).distance > result.get(id).distance+graph.getEdge(id, nb)) : "Graph with negative loop, there is no shortest path";
				if (result.get(id).distance == null) {
					//System.out.println("getrennter baum");
					continue;
				}
				else if (result.get(nb).distance > result.get(id).distance+graph.getEdge(id, nb)) { //wenn man trotz 3 for Schleifen noch schnellere Wege findet gibts negative loops
					return null;
					}
				}
		}	
		return result;
	}
	private static Hashtable<Integer, SSSPNode> init (Graph graph, int source){
		//Graph in Hashtable
		Hashtable<Integer, SSSPNode> result = new Hashtable<>(); //int = ID
		//init ausgehend vom Knoten source
		for (int id=0; id<graph.getNodeNumber(); id++) {
			if(id!=source) {
				SSSPNode node = new SSSPNode(null, null, id); //entfernung und vorgeaenger auf null setzen
				result.put(id, node);
			}
			else {
				SSSPNode node = new SSSPNode(null, 0, id); //entfernung auf 0 setzen
				result.put(id, node);
			}
		}
		return result;
	}
	
	private static Hashtable<Integer, SSSPNode> relax(Hashtable<Integer, SSSPNode> result, int id, int nb, int w){
		//System.out.println(result.get(id).distance);
		if (result.get(id).distance == null) { wenn id noch keine (=unendlich) Distanz hat
			return result;
		}
		else if (result.get(nb).distance == null) { //wenn nb noch keine Distanz hat
			result.get(nb).distance = result.get(id).distance + w;
			result.get(nb).predecessor = id;
		}
		else if (result.get(nb).distance > result.get(id).distance + w) { //vergleich, was schneller ist
			result.get(nb).distance = result.get(id).distance + w;
			result.get(nb).predecessor = id;
		}
		return result;
	}

	/**
	 * 
	 * @param graph the input graph
	 * @param source a valid Node ID of the graph
	 * @return for each node in the graph an entry in the hashmap which leads to a datatype storing the results
	 * 		   of the algorithm (as on the slides)
	 */
	public static Hashtable<Integer, SSSPNode> Dijkstra(Graph graph, int source) {
		int nodeNumb = graph.getNodeNumber();
		SSSPNode nil = new SSSPNode(null,null,-1);
		Hashtable<Integer, SSSPNode> result = new Hashtable<>();
		//init
		PriorityQueue<SSSPNode> Q = new PriorityQueue<>(nodeNumb, new SSSPNodeComparator());
		for(int i = nodeNumb-1;i>=0; i--) {
			if(i == source) {
				//System.out.println("einmal");
				Q.offer( new SSSPNode(null, 0, source)); //bei source distanz auf 0 setzen
			}else {
				Q.offer( new SSSPNode(null, null, i)); //sonst auf null (=unendlich) setzen
			}	
		}
		//While
		while(!Q.isEmpty()) {	//am anfang ist Q voll mit allen Knoten nach init
			SSSPNode u = Q.poll();
			//System.out.println(u.nodeNumber);
			for(int v :graph.getNeighbours(u.nodeNumber) ) {
					SSSPNode SSSP_v = Q.stream().filter(x -> x.nodeNumber==v ).findAny().orElse(nil);
					if(SSSP_v == nil) continue; //wenn keine Verbindung
					//zur Not Q mit iterator durchgehen
					//sonst relax
					if(u.distance == null) continue;
					assert u.distance != null : "OOps";
					if(SSSP_v.distance == null || SSSP_v.distance > (u.distance + graph.getEdge(u.nodeNumber, v))){  // weight = graph.get(int from, int to);	
						Q.remove(SSSP_v); //Knoten aus queue entfernen
						assert u.distance != null : "OOps";
						SSSP_v.distance = u.distance + graph.getEdge(u.nodeNumber, v); //distanz und predecessor setzen
						SSSP_v.predecessor=u.nodeNumber;
						Q.offer(SSSP_v); //?s
					}	
			}
			//node is black -> result
			result.put(u.nodeNumber,u);			
		}
		return result;
	}

	/**
	 * 
	 * @param graph input graph
	 * @return a hashmap with the nodeId´s as entries for "their" SSSPNodes; null
	 *         if negative cycle exists
	 */
	public static Hashtable<Integer, Hashtable<Integer, SSSPNode>> FloydWarshall(Graph graph) {
		int nodeNumber = graph.getNodeNumber();
		//		  source 			 target	  node of target
		Hashtable<Integer, Hashtable<Integer, SSSPNode>> result = new Hashtable<>();
		
		//Hashtable initialisieren
		for (int id=0; id<nodeNumber; id++) {
			result.put(id, init(graph, id)); //alle hashtables setten, source id -> distanz immer 
		}
		
		//alle Kanten durch gehen
		for (int from=0; from<graph.getNodeNumber(); from++) { //fuer alle knoten
			for (int to:graph.getNeighbours(from)) { //alle neibours to durchgenen
				SSSPNode node = result.get(from).get(to);
				node.predecessor = from; //vorgaener 
				node.distance = graph.getEdge(from, to); //und distanz auf edge gewicht setzen
			}
		}
		
		//resltichen Verbindungen durchgehen
		for (int over=0; over<nodeNumber; over++) { //Zwischenschritt, ueber den es evtl schneller geht
			for (int from=0; from<nodeNumber; from++) {
				for (int to=0; to<nodeNumber; to++) {
					if (result.get(from).get(over).distance == null || 
							result.get(over).get(to).distance == null) { // null abfangen
						continue;
					}					
					int overDist = result.get(from).get(over).distance + result.get(over).get(to).distance;
					if (result.get(from).get(to).distance == null || 
							result.get(from).get(to).distance > overDist) { //schnellerer Weg existiert
						SSSPNode node = result.get(from).get(to);
						node.predecessor = result.get(over).get(to).predecessor; //vorgaener 
						node.distance = overDist; //und distanz auf distanz uber Umweg over gewicht setzen
					}
				}
			}
		}
		
		//check for negative cycle in the diagonal of matrix
		for (int id=0; id<nodeNumber; id++) {
			if (result.get(id).get(id).distance <0) {
				return null;
			}
		}
//		//auf negative loops testen mit BellmanFord
//		for (int id=0; id<nodeNumber; id++) {
//			if (BellmanFord(graph, id) == null) {
//				return null;
//			}
//		}
		
		return result;
	}

}
