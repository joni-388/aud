package lab;

/**
 * Aufgabe H1a)
 * 
 * Abgabe von: <name>, <name> und <name>
 */

import java.util.Comparator;

import frame.SSSPNode;

public class SSSPNodeComparator implements Comparator<SSSPNode>
{

	@Override
	public int compare(SSSPNode nodeA, SSSPNode nodeB) //vergleicht, welcher Knoten naeher ist
	{	
		if(nodeA.distance == null && nodeB.distance == null ) return 0; //gleich weit
		if (nodeA.distance == null) return 1; //a = unendlich
		if (nodeB.distance == null) return -1; //b = unendlich
		if(nodeA.distance > nodeB.distance) { 
			return 1;
		}else if(nodeA.distance < nodeB.distance) {
			return -1;
		}else {
			return 0;
		}
	}

}
