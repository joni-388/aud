package lab;

/**
 * Aufgabe H1b)-H1c)
 *
 * Abgabe von: Magnus Dierking 2693156, Jonathan Lippert 2501536 und Tilman Froelich 2342436
 */

import java.util.Random;

import frame.ListNode;
import frame.TableEntry;

public class HashTable {
	
	protected int capacity;
	protected int hash_a;
	protected int hash_b;
	protected int size;
	protected LinkedList[] entryLists;
	
	 /**
     * The constructor
     * 
     * @param initialCapacity
     *            represents the initial size of the Hash Table.
     * 
     * The Hash-Table itself should be implemented in the array entryLists. When the
     * load factor exceeds 70%, the capacity of the Hash-Table should be
     * increased as described in the method rehash below.
     */
	public HashTable(int initialCapacity) {

		
		while(!isPrim(initialCapacity)) {
				initialCapacity++;
			}
		capacity = initialCapacity;
		size = 0;
		hash_a = (int)(Math.random() * (capacity-1))+1;
		hash_b = (int)(Math.random() * (capacity));
		entryLists = new LinkedList[capacity];
		for(int i = 0; i<entryLists.length;i++) {
			entryLists[i]= new LinkedList();
		}
		hash_a = (int)(Math.random() * (capacity-1))+1;
		hash_b = (int)(Math.random() * (capacity));
	}
	
	/**
	 * Search for an TableEntry with the given key. If no such TableEntry is found, return null.
	 */
	public TableEntry find(String key) {
		ListNode ausgabe;
		int position = hash(key);
		ausgabe = entryLists[position].searchKey(key);
		if(ausgabe == entryLists[position].nil()) {
			return null;
		}
		return ausgabe.entry();
	}
	
	/**
	 * Insert the given entry in the hash table. If there exists already an entry with this key,
	 * this entry should be overwritten. After inserting a new element, it might be necessary
	 * to increase the capacity of the hash table.
	 */
	public void insert(TableEntry entry) {
		int position = hash(entry.getKey());
		assert entryLists[position] != null: "ohhh noo!";
		ListNode same  =entryLists[position].searchKey(entry.getKey());
		if(same != entryLists[position].nil()) {
			same.entry().setData(entry.getData());
			return;
		}
		entryLists[position].insertBefore(entry, entryLists[position].head());
		size++;
		rehash();
	}
	
	/**
	 * Delete the TableEntry with the given key from the hash table and return the entry.
	 * Return null if key was not found.  
	 */
	public TableEntry delete(String key) {
		
		ListNode ausgabe;
		int position = hash(key);
		ausgabe = entryLists[position].searchKey(key);
		if(ausgabe == entryLists[position].nil()) {
			return null;
		}
		entryLists[position].delete(ausgabe);
		size--;
		return ausgabe.entry();
	}
	
	/**
	 * The hash function used in the hash table.
	 */
	public int hash(String x) {
		int asciiSum = 0;
		for(int i=0; i<x.length();i++) {
			int ascii = (int) x.charAt(i);
			asciiSum +=((((Math.pow(3, i)+i) % Math.pow(2, 16))*ascii) % Math.pow(2, 16));
		}
		asciiSum = univHashFct(asciiSum);
		if(asciiSum < 0) {
			asciiSum = asciiSum*(-1);
		}
		return asciiSum;
		
	}
	/**applies the universal hash function presented in the lectures
	 * 
	 * @param i value to apply the universal hash function to
	 * @return result after universal hash function was applied
	 */
	private int univHashFct(int i) {
		return ((hash_a*i+hash_b)% capacity);
	}
	
	/**
	 * Return the number of TableEntries in this hash table.
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Increase the capacity of the hash table and reorder all entries.
	 */
	private void rehash() {
		if((int)(Math.ceil(capacity * 0.7)) > size()) return;
		capacity = 5*capacity;
		while(!isPrim(capacity)) {
			capacity++;
		}
		hash_a = (int)(Math.random() * (capacity-1))+1;
		hash_b = (int)(Math.random() * (capacity));
		LinkedList[] newEntryLists = new LinkedList[capacity];
		
		for(int i = 0; i<newEntryLists.length;i++) {
			newEntryLists[i]= new LinkedList();
		}
		
		for(int i = 0; i< entryLists.length; i++) {
			while(entryLists[i].head() != entryLists[i].nil()) {
				
				TableEntry entry = entryLists[i].removeFirst().entry();
				int position = hash(entry.getKey());
				newEntryLists[position].insertBefore(entry, newEntryLists[position].head());	
			}
		}	
		entryLists = newEntryLists;
	}
	
	/**
	 * Return the current "quality" of the hash table. The quality is measured by calculating
	 * the maximum number of collisions between entries in the table (i.e., the longest linked
	 * list in the hash table).
	 */
	public int quality() {
		int maxLength =0;
		for(int i = 0; i< entryLists.length;i++) {
			if(entryLists[i] != null) {
				if(maxLength< entryLists[i].length()) {
					maxLength= entryLists[i].length();
				}
			}
		}
		return maxLength;
	}

	public int getHash_a() {
		return hash_a;
	}
	
	public int getHash_b() {
		return hash_b;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public LinkedList[] getEntryLists() {
		return entryLists;
	}
	
	/**
	 *  
     * Testet, ob Zahl eine Primzahl ist. 
     *
	 * @param value zu prüfende Zahl
	 * @return boolean, je nachdem ob value eine Primzahl oder nicht
	 */
    public boolean isPrim(int value) { 
        if (value <= 2) { 
            return (value == 2); 
        } 
        for (int i = 2; i * i <= value; i++) { 
            if (value % i == 0) { 
                return false; 
            } 
        } 
        return true; 
    }
//	/**
//	 * calculates the loadFactor of entryLists
//	 * @return loadFactor
//	 */
//    public int getLoadFactor() {
//    	int loadFactor =0;
//		for(int i = 0; i< entryLists.length;i++) {
//			if(entryLists[i].head() != entryLists[i].nil()) {
//				loadFactor++;
//			}
//		}
//		return loadFactor;
//    }
	

}
