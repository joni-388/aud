package lab;

/**
 * Aufgabe H1a)
 *
 * Abgabe von: Magnus Dierking 2693156, Jonathan Lippert 2501536 und Tilman Froelich 2342436
 */

import frame.ListNode;
import frame.TableEntry;

public class LinkedList {
	
	private ListNode _head;
	private ListNode _nil;
	
	public LinkedList() {
		_nil = new ListNode(null, null, null);
		_nil.setNext(_nil);
		_nil.setPrev(_nil);
		_head = _nil;
	}
	
	public ListNode head() {
		return _head;
	}
	
	public ListNode nil() {
		return _nil;
	}
	
	/**
	 * Return the number of elements in the linked list.
	 */
	public int length() {
		// TODO
		int length = 0;
		ListNode node = this._head; //start at head
		while (node != this._nil){ //continue to count while there is another node that is not _nil
			node = node.next();
			length ++;
		}
		return length;
	}
	
	/**
	 * Insert an entry into the linked list at the position before the given node.
	 */
	public void insertBefore(TableEntry entry, ListNode node) {
		// TODO
		if(this._head!=this._nil){ //liste nicht leer
			ListNode insert = new ListNode(entry, node.prev(), node); //create "insert" node with TableEntry entry
			if(node.prev()!=null) {
				node.prev().setNext(insert); //set next of previous node				
			}
			node.setPrev(insert); //set previous of next node "node"
			if(node==this._head) { //sonderfall als head einfuegen
				this._head=insert;
			}
		}			
		else {//if entry of head ist null = leere liste
			ListNode insert = new ListNode(entry, null, this._head.next()); //create node with TableEntry entry
			this._head = insert; //replace head
			insert.next().setPrev(insert);
		}
	}
	
	/**
	 * Append an entry to the end of the list.
	 */
	public void append(TableEntry entry) {
		// TODO
		if (this._head!=this._nil) { //liste nicht leer
			ListNode node = this._head; //start at head
			while (node.next() != this._nil){ //continue to count while there is another node that is not _nil
				node = node.next();
			}
			ListNode insert = new ListNode(entry, node, node.next()); //create node with TableEntry entry
			node.setNext(insert); //inset in LinkedList
		}
		else {//if entry of head ist null = leere liste
			ListNode insert = new ListNode(entry, _nil, _nil); //create node with TableEntry entry
			_head = insert; //replace head
		}
	}
	
	/**
	 * Delete the given node from the linked list.
	 */
	public void delete(ListNode node) {
		// TODO
		if(node == _head) { //anfang der List
		    _head = _head.next();
		}else if(node.next() == _nil) { //ende der List
			node.prev().setNext(_nil);
		}else { //Normalfall
			node.next().setPrev(node.prev());
		    node.prev().setNext(node.next());
		}
	}
	
	/**
	 * searches the linkedList for a node with specific key
	 * @param key to look for
	 * @return the node with the specific key
	 */
	public ListNode searchKey(String key) {
		ListNode node = this._head;
		while(node != _nil ) {
			if(node.entry().getKey() == key ) {
				return node;
			}else {
				node = node.next();
			}
		}
		return _nil; //wenn nicht vorher zurueckgegeben wurde
	}
	
	/**Entfernt erstes Element der Liste und gibt dieses zurück
	 * 
	 * @return entferntes erstes Element
	 */
	public ListNode removeFirst() {
		if(_head == _nil) return _nil;
		if(_head.next() == null) { //leere List
			ListNode temp = _head;
			_head = _nil;
			return temp;
		}else {
			ListNode temp = _head;
			_head = _head.next();
			return temp;
		}
		
	}
}
