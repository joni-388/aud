package lab;

/**
 * Aufgabe H1a)
 * 
 * Abgabe von: <name>, <name> und <name>
 */

public class Card {

	// DO NOT MODIFY
	public enum Suit {
		Hearts, Diamonds, Clubs, Spades
	}

	// DO NOT MODIFY
	public int value;
	public Suit suit;

	// DO NOT MODIFY
	public Card() {
	}

	// DO NOT MODIFY
	/**
	 * Initialize a Card with a suit and a value
	 */
	public Card(int value, Suit suit) {
		this.value = value;
		this.suit = suit;
	}

	// DO NOT MODIFY
	/**
	 * Create a new card from an existing card
	 */
	public Card(Card other) {
		this.value = other.value;
		this.suit = other.suit;
	}

	/**
	 * Return a printable representation of the card
	 */
	public String toString() {
		return value + ";" + suit;
	}

	/**
	 * Compare two card objects. Return -1 if this is deemed smaller than the object
	 * other, 0 if they are deemed of identical value, and 1 if this is deemed
	 * greater than the object other.
	 * 
	 * @param other The object we compare this to.
	 * @return -1, 0 or 1
	 */
	public int compareTo(Card other) {
<<<<<<< HEAD
		if (value > other.value) {
			return 1;
		} else if (value < other.value) {
			return -1;
		} else {
			return compareSuit(other);
		}

	}

	public int compareSuit(Card other) {
		int suitValOther = 0;
		int suitValThis = 0;
		switch (this.suit) {
		case Diamonds:
			suitValThis = 1;
			break;
		case Hearts:
			suitValThis = 2;
			break;
		case Spades:
			suitValThis = 3;
			break;
		case Clubs:
			suitValThis = 4;
			break;

		}
		switch (other.suit) {
		case Diamonds:
			suitValOther = 1;
			break;
		case Hearts:
			suitValOther = 2;
			break;
		case Spades:
			suitValOther = 3;
			break;
		case Clubs:
			suitValOther = 4;
			break;

		}
		int x = suitValThis - suitValOther;
		if (x < 0) {
			return -1;
		} else if (x > 0) {
			return 1;
		} else {
			return 0;
		}
=======
		// TODO: implement
		if(other.value > value) {
			return -1;
		}
		else if(other.value < value) {
			return 1;
		}else {
		    if(other.suit == suit) {
		    	return 0;
		    }else if(suit == Suit.Diamonds) {
		    	return -1;
		    }else if(other.suit == Suit.Diamonds) {
		    	return 1;
		    }else if(suit == Suit.Clubs) {
		    	return 1;
		    }else if(other.suit == Suit.Clubs) {
		    	return -1;
		    }else if(suit == Suit.Hearts) {
		    	return -1;
		    }else {
		    	return 1;
		    }
		}
	}
	
	private int suitValue (Card card){
		if (card.suit == Suit.Clubs){ return 4;}
		else if (card.suit == Suit.Spades){ return 3;}
		else if (card.suit == Suit.Hearts){ return 2;}
		else { return 1;}
>>>>>>> Jonathan
	}
	
}
