package lab;

/**
 * Aufgabe H1b)
 * 
 * Abgabe von: <name>, <name> und <name>
 */

import frame.SortArray;

public class HybridSort {

	/**
	 * Sort the given array using a hybrid method of Quick Sort and Insertion Sort.
	 * 
	 * @param array The array to sort.
	 * @param k     Parameter k when we switch from Quick Sort to Insertion Sort: If
	 *              the size of the subset which should be sorted is less than k,
	 *              use Insertion Sort, otherwise keep on using Quick Sort.
	 */
	public void sort(SortArray array, int k) {
<<<<<<< HEAD
		assert (k >= 0);
		quickSort(array, 0, array.getNumberOfItems() - 1, k);
=======
		assert(k>=0);

		// TODO: Implement
		quickSort(array, 0, array.getNumberOfItems()-1, k);
>>>>>>> Jonathan
	}

	protected void quickSort(SortArray array, int p, int r, int k) {
		/*if (p < r) {
			if (r - p < k) {
				insertionSort(array, p, r);
			} else {
				int q = partition(array, p, r);
				quickSort(array, p, q - 1, k);
				quickSort(array, q + 1, r, k);
			}
		}*/
		if(p<r) {
			int q= partition(array,p,r);
			if(q-p<k) {
				insertionSort(array,p,q-1);
			}else {
				quickSort(array,p,q-1,k);
			}
			if(r-q<k) {
				insertionSort(array,q+1,r);
			}else {
				quickSort(array,q+1,r,k);
			}
		}
			
	}
<<<<<<< HEAD

	protected int partition(SortArray array, int p, int r) {
		Card pivot = array.getElementAt(getPivot(array, p, r));
		int i = p-1;
		for (int j = p + 1; j <= r; j++) {
			if (array.getElementAt(j).compareTo(pivot) <0) {
				i = i + 1;
				Card temp = array.getElementAt(j);
				array.setElementAt(j, array.getElementAt(i));
				array.setElementAt(i, temp);
			}
		}
		Card temp = array.getElementAt(i + 1);
		array.setElementAt(i + 1, array.getElementAt(p));
		array.setElementAt(p, temp);
		return i + 1;
	}

	protected int getPivot(SortArray array, int p, int r) {
		return p;
	}

	protected void insertionSort(SortArray array, int p, int r) {
		for (int j = p ; j <= r; j++) {
			Card key = array.getElementAt(j);
			int i = j - 1;
			while (i >= 0 && array.getElementAt(i).compareTo(key) ==1) {
				array.setElementAt(i + 1, array.getElementAt(i));
				i = i - 1;
			}
			array.setElementAt(i + 1, key);
		}
	}
=======
>>>>>>> Jonathan
}
