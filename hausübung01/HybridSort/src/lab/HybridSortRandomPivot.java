package lab;

import java.util.Random;

import frame.SortArray;

/**
 * Use a random pivot within Quick Sort.
 */
public class HybridSortRandomPivot extends HybridSort {
	@Override
	protected int getPivot(SortArray array,int p,int r) {
		Random random = new Random();
		int intervall = r-p;
		int pivotIndex =random.nextInt(intervall+1)+p;
		return pivotIndex;
	}
	@Override
	protected int partition(SortArray array, int p, int r) {
		int pivotIndex = getPivot(array,p,r);
		Card pivot = array.getElementAt(pivotIndex);
		int i = p-1;
		for (int j = p; j <= r; j++) {
			if(j!=pivotIndex)
			if (array.getElementAt(j).compareTo(pivot) <= 0) {
				i = i + 1;
				if(i==pivotIndex)i++;
				Card temp = array.getElementAt(j);
				array.setElementAt(j, array.getElementAt(i));
				array.setElementAt(i, temp);
			}
		}
		Card temp = array.getElementAt(i + 1);
		array.setElementAt(i + 1, array.getElementAt(pivotIndex));
		array.setElementAt(pivotIndex, temp);
		return i + 1;
	}
	

	


}
