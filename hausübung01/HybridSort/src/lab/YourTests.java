package lab;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import frame.CardTestfileReader;
import frame.SortArray;

/**
 * Use this class to implement your own tests.
 */
class YourTests {
	CardTestfileReader reader1;
	CardTestfileReader reader2;
	CardTestfileReader reader3;
	@BeforeEach
	void setUp() {
		// reader1 =new CardTestfileReader("\tests\public\TestFile1");
		// reader2 =new CardTestfileReader("TestFile2.txt");
		// reader3 =new CardTestfileReader("TestFile3.txt");
	}

	@Test
	void test1() {
		SortArray testArray1 = new SortArray(reader1.readFile());
		HybridSort hybridSort = new HybridSort();
		hybridSort.sort(testArray1,0);
		assert(testArray1.getNumberOfItems()>=1);
		for(int i=0; i<testArray1.getNumberOfItems()-1;i++) {
			assert(
					testArray1.getElementAt(i).compareTo(testArray1.getElementAt(i+1))<=0
					);
		}		
	}

}
