package lab;

/**
 * Aufgabe H1c)
 * 
 * Abgabe von: <name>, <name> und <name>
 */

import java.util.ArrayList;
import frame.SortArray;

public class HybridOptimizer {

	/**
	 * Find the optimal value k of the HybridSort algorithm for
	 * the given data. Note that we assume that the first local minimum is
	 * the global minimum.
	 * @param testData Data on which the optimal k should be calculated
	 * @return the optimal k
	 */
	public static int optimize(ArrayList<Card> testData) {
		HybridSort hybridSort=new HybridSort();
		for(int k=0;k<testData.size();k++) {
			SortArray arrayQuick =new SortArray(testData);
			SortArray arrayInsert=new SortArray(testData);
			hybridSort.sort(arrayQuick, k);
			hybridSort.sort(arrayInsert,k);
			int operationsQuick =arrayQuick.getReadingOperations()+arrayQuick.getWritingOperations();
			int operationsInsert=arrayInsert.getReadingOperations()+arrayInsert.getWritingOperations();
			if(operationsInsert<operationsQuick) {
				return k;
			}
		}
		return testData.size();
	}

}
