package lab;

/**
 * Aufgabe H1b)
 * 
 * Abgabe von: Magnus Dierking 2693156, Jonathan Lippert 2501536 und Tilman Froelich 2342436
 */

import frame.TreeNode;
import frame.TreeNode.NodeColor;
import frame.Process;

public class CompletelyFairScheduler {
	
	private RedBlackTree tree;
	private int windowMaxLength;
	
	/**
	 * Creating a new CompletelyFairScheduler
	 * @param windowMaxLength is the maximal length of one execution window
	 */
	public CompletelyFairScheduler(int windowMaxLength) {
		tree = new RedBlackTree();
		this.windowMaxLength = windowMaxLength;
	}
	
	/**
	 * Distribute execution windows to the processes.
	 * @param windows The number of execution windows to distribute
	 */
	public void run(int windows) {
		//run all windows
		for (int i=0; i<windows; i++) {			// fuer anzahl an freien windows wird je ein prozess ausgefuehrt solange noch welche warten
			TreeNode temp = tree.minimum();		// process mit kleinster execution time wird als naechstes ausgefuehrt
			if(temp == null ) return;			// falls baum leer ist ist run zu ende
			Process p = temp.value;
			p.run(windowMaxLength);				// aktueller process wird ausgefuehrt
			tree.delete(temp);				// process mit alter execution time entfernt
			if( p.finished() != true) {		// falls process nicht fertig wird er wieder eingereiht mit neuer execution time
				add(p);
			}
		}
	}
	
	/**
	 * Add a process to the Scheduler.
	 */
	public void add(Process process) {
		TreeNode newNode = new TreeNode();
		newNode.value = process;					
		newNode.key = process.executionTime();			// neuer key ist die neue execution Time des Prozesses
		while (tree.search(newNode.key) != null) {  // solange es einen Process mit gleicher exectuion Time/ key gibt wird key eins erhoeht
			newNode.key++;
		}
		tree.insert(newNode);
	}
	
	// DO NOT MODIFY
	// used for the tests
	public RedBlackTree getTree() {
		return tree;
	}

}
