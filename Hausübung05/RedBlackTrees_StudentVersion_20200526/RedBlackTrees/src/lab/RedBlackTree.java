package lab;

/**
 * Aufgabe H1a)
 * 
 * Abgabe von: Magnus Dierking 2693156, Jonathan Lippert 2501536 und Tilman Froelich 2342436
 */

import frame.TreeNode;
import static frame.TreeNode.NodeColor.RED;

import java.awt.Color;

import static frame.TreeNode.NodeColor.BLACK;

/**
 * An implementation of a Black-Red-Tree
 */
public class RedBlackTree {

	private TreeNode _root;
	private TreeNode _nil;

	public RedBlackTree() {
		_nil = new TreeNode();
		_root = _nil;
	}

	public TreeNode root() {
		return this._root;
	}

	public TreeNode nil() {
		return this._nil;
	}

	/**
	 * Return the node with the given key, or nil, if no such node exists.
	 */
	public TreeNode search(int key) {
		TreeNode x = _root; // Der aktuelle Knoten wird auf die Wurzel gesetzt: Die Suche beginnt bei der
							// Wurzel
		while (x != _nil && x.key != key) { // Falls der aktuelle Knoten nil ist, ist die Suche beendet, es wurde kein
											// Knoten gefunden. Sollte jedoch der betrachtete key gleich dem gesuchten sein, ist die Suche beendet.
			if (x.key > key) { // Bricht die Schleife nicht ab, so wird im Linken Teilbaum weiter gesucht,
								// falls der gesuchte wert kleiner als der Wert des aktuellen Knotens ist.
				x = x.left;
			} else {
				x = x.right; // Falls der gesuchte Wert groesser als der Aktuelle ist, wird im rechten Teilbaum weitergesucht.
			}
		}
		return x;
	}

	/**
	 * Return the node with the smallest key
	 */
	public TreeNode minimum() {
		return minimumSubtree(_root);
	}

	/**
	 * Return the node with the smallest key in the subtree that has x as root node.
	 */
	public TreeNode minimumSubtree(TreeNode x) {
		if (x == _nil)
			return x; // Da in einem Binaerbaum der Knoten mit dem kleinsten Wert ganz Links zu finden sein wird, wird das am weitesten links liegende Kind zurueckgegeben.
		while (x.left != _nil) {
			x = x.left;
		}
		return x;
	}

	/**
	 * Return the node with the greatest key.
	 */
	public TreeNode maximum() {
		return maximumSubtree(_root);
	}

	/**
	 * Return the node with the greatest key in the subtree that has x as root node.
	 */
	public TreeNode maximumSubtree(TreeNode x) {
		if (x == _nil)
			return _nil; // Da in einem Binaerbaum der Knoten mit dem groessten Wert sich ganz rechts zu finden sein wird, wird das am weitesten rechts liegende Kind zurueckgegeben.

		while (x.right != _nil) {
			x = x.right;
		}
		return x;
	}

	/**
	 * Insert newNode into the tree.
	 */
	public void insert(TreeNode newNode) {
		newNode.left = _nil; // die Kinder des Neuen Knoten werden auf default wert gesetzt (_nil), sonst wird eine NullPointerException geworfen.
		newNode.right = _nil;
		TreeNode x = _root; // Pointer fuer den Knoten
		TreeNode px = _nil; // Pointer fuer den Vaterknoten
		while (x != _nil) { // Abbruch, wenn Blatt erreicht, d.h. Kind von betrachtetem Knoten x ist _nil
			px = x; // x ist nun parent da x im  naechsten Schritt auf Kind von x gesetzt wird
			if (x.key > newNode.key) { // falls key-Wert des betrachteten Knotens groesser als der des einzufuegenden Knotens wird links weitergesucht
				x = x.left;
			} else {
				x = x.right; // falls key-Wert des betrachteten Knotens kleiner als der des einzufuegenden Knotens wird rechts weitergesucht
			}
		}
		newNode.p = px; // parent von newNode auf Parent von x da x nun _nil muss px ein Blatt des
						// Baumes sein
		if (px == _nil) {
			_root = newNode; // falls root nil war ist nun newNode neue Wurzel
		} else {
			if (px.key > newNode.key) { // ansonsten wird newNode links an das Blatt angehaengt, wenn key-Wert kleiner als
										// Blatt
				px.left = newNode;
			} else {
				px.right = newNode; // und rechts vom Blatt sonst.
			}
		}
		newNode.color = RED;// TreeNode.NodeColor.RED; // Das neue Blatt wird rot gefaerbt, da dies das Fixup erleichtert
		fixColorsAfterInsertions(newNode); // Die rot-schwarz-Ordnung wird wieder hergestellt.
	}

	/**stellt Ordnung im rot schwartz Baum nach Einfuegen eines Knotens wieder her
	 * 
	 * @param node Knoten der eingefuegt wurde
	 */
	private void fixColorsAfterInsertions(TreeNode node) {
		while (node.p.color == RED) { // wird solange durchgefuehrt wie betrachteter Knoten rot ist
			if (node.p == node.p.p.left) { // wenn Vater von node ein linkes Kind ist
				TreeNode y = node.p.p.right; // dann ist y das rechte kind vom Vaterknoten von node (y ist Onkel)
				if (y != _nil && y.color == RED) { // Szenario 1: node hat einen roten Onkel - Ordnung verletzt, da roter Knoten nun 1 rotes und 1 schwartzes Kind hat(bzw. _nil Kind)
					node.p.color = BLACK; //Vater wird umgefaerbt
					y.color = BLACK;  // Onkel wird umgefaerbt
					node.p.p.color = RED; // Grossvater wird umgefaerbt
					node = node.p.p; // Pointer wird auf Grossvater gesetzt, da es sich um Subtree handeln koennte und so die Ordnung auf hoeherer Ebene nun verletzt sein koennte
				} else {                    // Szenario 2: node hat schwartzen Onkel - Ordnung verletzt, da nun roter Vater von node schwartzes und rotes Kind hat
					if (node == node.p.right) { //Szenario 2a: Knoten ist rechtes Kind, Vater linkes Kind
						node = node.p;    // Pointer auf Vaterkoten, da dieser rotiert werden muss, um Ordnung wiederherzustellen
						rotateLeft(node);  //rotiere Vaterknoten in entgegengesetzte Richtung (links, da Knoten rechtes Kind)     
					}
					node.p.color = BLACK;  // Szenario 2b: Knoten ist linkes Kind, Vater ebenfalls - zusaetzliches umfaerben notwendig, da nach rotation Vater von node rotes und schwartzes Kind hat
					node.p.p.color = RED;  // umfaerben, um Regeln wiederherzustellen
					rotateRight(node.p.p);  //rotiere Grossvater in entgegengesetzte Richtung
				}
			} else {                      // ansonsten ist Vater von node ein rechtes Kind, da es sich um binaeren Baum handelt
				TreeNode y = node.p.p.left; // dann ist y das linke kind vom Vaterknoten von node (Onkel)
				if (y != _nil && y.color == RED) { // Szenario 1: node hat einen roten Onkel  - Ordnung verletzt, da roter Knoten nun 1 rotes und 1 schwartzes Kind hat(bzw. _nil Kind)
					node.p.color = BLACK;  //Vater wird umgefaerbt
					y.color = BLACK;  // Onkel wird umgefaerbt
					node.p.p.color = RED;  // Grossvater wird umgefaerbt
					node = node.p.p;  // Pointer wird auf Grossvater gesetzt, da es sich um Subtree handeln koennte und so die Ordnung auf hoeherer Ebene nun verletzt sein koennte
				} else {                 // Szenario 2: node hat schwartzen Onkel - Ordnung verletzt, da nun roter Vater von node schwartzes und rotes Kind hat
					if (node == node.p.left) { // Szenario 2a: Knoten ist linkes Kind, Vater rechtes Kind
						node = node.p;  // Pointer auf Vaterknoten, da dieser rotiert werden muss, um Ordnung wiederherzustellen
						rotateRight(node);  //rotiere Vaterknoten in entgegengesetzte Richtung (rechts, da Knoten linkes Kind)   
					}
					node.p.color = BLACK;    // Szenario 2b: Knoten ist rechtes Kind, Vater ebenfalls - zusaetzliches umfaerben notwendig, da nach rotation Vater von node rotes und schwartzes Kind hat
					node.p.p.color = RED;  // umfaerben, um Regeln wiederherzustellen 
					rotateLeft(node.p.p);  // rotiere Grossvater in entgegengesetzte Richtung
				}
			}
		}
		_root.color = BLACK;  // Szenario 0: node ist eine Wurzel, einfaches umfaerben auf schwartz stellt Ordnung her
	}

	/**veraendert Struktur des Baumes durch Rechtsdrehung
	 * 
	 * @param node Knoten, der nach links rotiert werden soll
	 */
	private void rotateRight(TreeNode node) {
		TreeNode y = node.left;//setze y auf linkes Kind
		node.left = y.right;//linker Teilbaum von node wird rechter Teilbaum von y 
		if (y.right != _nil) {//wenn rechter Teilbaum von y vorhanden
			y.right.p = node;// wird Vater des rechten Kindes von y auf node gesetzt
		}
		y.p = node.p; // setze y Vater auf nodes Vater
		if (node.p == _nil) { //falls node Wurzel war, ist dies nun y
			_root = y;
		} else {
			if (node == node.p.right) { //war node rechtes Kind
				node.p.right = y; // wird y das linke Kind von nodes Vater
			} else {
				node.p.left = y; //ansonsten analog links
			}
		}
		y.right = node;//node wird Wurzel des linken Teilbaumes von von y
		node.p = y; // y wird vater von node
	}

	/**veraendert Struktur des Baumes durch Linksdrehung
	 * 
	 * @param node Knoten, der nach links rotiert werden soll
	 */
	private void rotateLeft(TreeNode node) {
		TreeNode y = node.right; //setze y auf rechtes Kind
		node.right = y.left; //rechter Teilbaum von node wird linker Teilbaum von y 
		if (y.left != _nil) { // wenn linker Teilbaum von y vorhanden
			y.left.p = node; // wird Vater des linken Kindes von y auf node gesetzt
		}
		y.p = node.p; // setze y Vater auf nodes Vater
		if (node.p == _nil) { // falls node Wurzel war, ist dies nun y
			_root = y;
		} else {
			if (node == node.p.left) { // war node linkes Kind
				node.p.left = y;// wird y das linke Kind von nodes Vater
			} else {
				node.p.right = y; // ansonsten analog rechts
			}
		}
		y.left = node; //node wird Wurzel des linken Teilbaumes von von y
		node.p = y; // y wird vater von node
	}

	/**
	 * Delete node toDelete from the tree.
	 */
	public void delete(TreeNode toDelete) {
		TreeNode sibling = _nil;
		TreeNode x = _nil;
<<<<<<< HEAD
		if (toDelete.left == _nil) { // falls linker teilbaum leer wird rechtes Kind an den elternknoten angeh�ngt
			x = toDelete.right;
			toDelete.right.color=BLACK;	// wenn toDelete nur ein kind hat ist dieses Rot wir f�rben es schwarz und alle teilb�ume gehen dann durch diesen knoten und die schwarzh�he bleibt erf�llt
			transplant(toDelete, toDelete.right); // hier in transplant implementiert falls rechtes kind auch _nil
													// erstezt _nil den knoten
		} else {
			if (toDelete.right == _nil) { // falls rechter Tailbaum leer wird linkes Kind an elternknoten angeh�ngt
				x=toDelete.left;
				toDelete.left.color=BLACK;	// selbes argument: to delete nur ein kind dieses ist rot -> wird schwarz gef�rbt -> schwarzregel erf�llt
=======
		if (toDelete.left == _nil) { // falls linker teilbaum leer wird rechtes Kind an den elternknoten angehaengt
			toDelete.right.color=BLACK;	// wenn toDelete nur ein kind hat ist dieses Rot wir faerben es schwarz und alle teilbaeume gehen dann durch diesen knoten und die schwarzhaehe bleibt erfaellt
			transplant(toDelete, toDelete.right); // hier in transplant implementiert falls rechtes kind auch _nil
													// erstezt _nil den knoten
		} else {
			if (toDelete.right == _nil) { // falls rechter Tailbaum leer wird linkes Kind an elternknoten angehaengt
				toDelete.left.color=BLACK;	// selbes argument: to delete nur ein kind dieses ist rot -> wird schwarz gefaerbt -> schwarzregel erfaellt
>>>>>>> master
				transplant(toDelete, toDelete.left);
			} else { 
				TreeNode y = toDelete.right; // wir suchen kleinsten nachfahren also linkestes kind des rechten
												// kindes
				while (y.left != _nil) {
					y = y.left;
				}
				x = y.right;
				TreeNode.NodeColor temp = y.color;		// y und toDelete tauschen benutzerdaten also farben aus
				y.color= toDelete.color;
				if (y.p != toDelete) { // wenn y kind von toDelete ist kann man sich dei nachfolgenden drei
										// operationen
										// sparen, da das Ziel von ihnen schon erreicht ist

					/*if(temp != RED) {
						if( y.right.right != _nil || y.right.left != _nil) {  // y.right ist kein blatt 	
							y.right.color = BLACK; 	// da y schwarz und nur ein kind hat ist kind rot und tritt nun an die stelle von y und wird schwarz
						}else {		// y ist schwarzes blatt
							//TreeNode sibling;
							x = y.right;
							if(y == y.p.left) {
								sibling = y.p.right;
							}else {
								sibling = y.p.left;
							}
							//fixColorsAfterDeletion(x, sibling);	
							
						}
					}*/
					transplant(y, y.right); // rechter Teilbaum von y wird an elternkonten von y angehaengt, somit
											// ist y
											// "frei" zum vertauschen da linkes und rechtes Kind _nil ist
					y.right = toDelete.right; // rechter Teilbaum des zu Loeschenden Knoten wird an y angehaengt, der
												// nun
												// an der stelle des zu loeschenden Knoten sitzt
					y.right.p = y; // und umgekehrt ist y nun parent vom rechten teilbaum
				}
				transplant(toDelete, y); // nun wird y mit sammt teilbaum an die stelle von toDelete Gesetzt, d.h.
											// an
											// den elternknoten von toDelete angehaengt
				y.left = toDelete.left; // Linker Teilbaum wird an y angehaengt. y.links ist vorher null da y
										// kleinster
										// nachfolger von toDelete
				y.left.p = y; // und umgekehrt zeit linkerteilbaum mit p auf y damit ist toDelete erfolgreich
<<<<<<< HEAD
								// gel�scht (von y ersetzt) und teilb�ume korrekt angeh�ngt
		
				// f�r RED BLACK TREE FixUp
				if(toDelete.color == RED && (temp == RED || y == _nil)) {
=======
								// geloescht (von y ersetzt) und teilbaeume korrekt angehaengt
				
				//fixColorsAfterDeletion(x, sibling);
				if(toDelete.color == RED && (temp == RED || y == _nil)) { //Faelle abpruefen in denen bereits alle Farbregeln erfuellt sind
>>>>>>> master
					return;
				}else if( temp == RED) {
					return;
				}else if(x == _nil){
					return;
				}else {
					TreeNode s; //Geschwisterknoten herausfinden
					if(x == x.p.left) {
						s = x.p.right;
					}else {
						s = x.p.left;
					}
<<<<<<< HEAD
					fixColorsAfterDeletion2(x,s);
				}	
=======
					fixColorsAfterDeletion2(x,s); //Farbregeln fuer Rot-Schwarz Baume wiederherstellen
				}				
>>>>>>> master
			}
		}
	}

	/**
	 * Corrects colors until rules of red-black-trees are followed
	 * @param x node	
	 * @param s sibling
	 */
	private void fixColorsAfterDeletion2(TreeNode x,TreeNode s) {
		boolean xRightChild = false; 

		if(x.p.right == x) { //onkel wird geprueft
			xRightChild = true;
		}
		int cases = -1; //verschiedene Faelle zum wiederherstellen der Baumstruktur werden geprueft
		if(x.color == RED) { //roter knoten
			cases = 0;
		}else if( s.color == RED) { //gewschisterknoten andersfarbig
			cases = 1;
		}else if( s.left.color == BLACK && s.right.color == BLACK) { 
			cases = 2;
		}else if(xRightChild) {
			if(s.right.color == RED && s.left.color == BLACK) {
				cases = 3;
			}else if(s.left.color == RED) {
				cases = 4;
			}
		}else {
			if(s.left.color == RED && s.right.color == BLACK) {
				cases = 3;
			}else if(s.right.color == RED) {
				cases = 4;
			}
		}
		switch(cases) {
		case 0 : //Wurzelknoten auf schwarz setzen
			x.color = BLACK;
			break;
		case 1 :
			s.color = BLACK;
			x.p.color = BLACK;
			if(xRightChild) {
				rotateRight(x.p);
			}else {
				rotateLeft(x.p);
			}
			if(x.p.right == x) {
				s = x.p.left;
			}else {
				s = x.p.right;
			}
			assert s != null : "OOps1";
			fixColorsAfterDeletion2(x, s); //iterativ auf neu entstandene Fehler pruefen
			break;
		
		case 2 : 
			s.color = RED;
			x=x.p;
			if(x.color == RED) {
				x.color = BLACK;
				return;
			}
			if(x == _root) {
				x.color = BLACK;
				return;
			}
			if(x.p.right == x) {
				s = x.p.left;
			}else {
				s = x.p.right;
			}
			fixColorsAfterDeletion2(x, s); //iterativ auf neu entstandene Fehler pruefen
			break;
		
		case 3 :
			if(x.p.right == x) {
				s.right.color = BLACK;
			}else {
				s.left.color = BLACK;
			}
			s.color = RED;
			if(x.p.right == x) {
				rotateLeft(s);
			}else {
				rotateRight(s);
			}
			if(x.p.right == x) {
				s=x.p.left;
			}else {
				s=x.p.right;
			}
			
		case 4 : 
			s.color = x.p.color;
			x.p.color = BLACK;
			if(x.p.right == x) {
				s.left.color = BLACK;
			}else {
				s.right.color = BLACK;
			}
			if(x.p.right == x) {
				rotateRight(x.p);
			}else {
				rotateLeft(x.p);
			}
			return;
		}
		
	}
	/**
	 * Transplant haengt Teilbaum v an Elternkonten von u an
	 * 
	 * @param u Knoten an dessen Elternknoten v angehaengt wird
	 * @param v anzuhaengender Teilbaum
	 */
	private void transplant(TreeNode u, TreeNode v) {
		if (u.p == _nil) { // falls u Wurzel wird v neue Wurzel
			_root = v;
		} else {
			if (u == u.p.left) { // falls u linkes Kind
				u.p.left = v; // wird v als linkes Kind an parent von u angehaengt
			} else {
				u.p.right = v; // sonst als rechtes kind an parent angehaengt
			}
		}
		if (v != _nil) { // falls v = _nil sind wir fertig
			v.p = u.p; // ansonsten wird auch parent von v auf parent von u gesetzt. ist v neue wurzel
						// wird parent auf _nil gesetzt
		}
	}

//	private void fixColorsAfterDeletion(TreeNode x, TreeNode sibling) {
//		if( x != _nil && sibling != _nil) {
//			if(x.color == RED) { // Ersatz ist rot, einfachers umfaerben genuegt
//				x.color = BLACK;
//			}else {
//				if(sibling.color == RED) { // Geschwister von Ersatz ist rot
//					sibling.color = BLACK;
//					x.p.color = RED;
//					if(x.p.left == x) {
//						rotateLeft(x.p);
//						fixColorsAfterDeletion(x, x.p.right);
//					}else {
//						rotateRight(x.p);
//						fixColorsAfterDeletion(x, x.p.left);
//					}		
//				}else {
//					if(sibling.left.color == BLACK && sibling.right.color == BLACK) {
//						sibling.color = RED;
//						x = x.p;
//						if(x.color == RED) {
//							x.color = BLACK;
//						}else {
//							TreeNode newSibling;  // setzen des neuen Geschwisterknotens 
//							if(x.p.left == x) {
//								newSibling = x.p.right;
//							}else {
//								newSibling = x.p.left;
//							}
//							fixColorsAfterDeletion(x, newSibling); // weitersortieren
//						}	
//					}else { // Ersatz schwartz, Geschwister schwartz, Geschwisterkinder verschieden
//						
//						
//						
//						
//						if(x.p.left == x) { // Ersatz ist linkes Kind
//							if(sibling.left.color == RED) { //Geschwister linkes Kind rot 4
//								sibling.color = x.p.color;
//								x.p.color = BLACK;
//								sibling.right.color = BLACK;
//								rotateLeft(x.p);
//							}else { //Geschwister linkes Kind ist rot, rechtes schwartz 3
//								sibling.left.color = BLACK;
//								sibling.color = RED;
//								rotateRight(sibling);
//								sibling = x.p.right;
//								//unsafe
//								fixColorsAfterDeletion(x, sibling);
//							}
//							
//						}else { // Ersatz ist rechtes Kind
//							if(sibling.right.color == RED) { //Geschwister rechtes Kind rot 4
//								sibling.color = x.p.color;
//								x.p.color = BLACK;
//								sibling.left.color = BLACK;
//								rotateRight(x.p);
//								
//							}else { //Geschwister rechtes Kind rot, linkes schwartz 3
//								sibling.right.color = BLACK;
//								sibling.color = RED;
//								rotateLeft(sibling);
//								sibling = x.p.left;
//								// unsafe
//								fixColorsAfterDeletion(x, sibling);
//								
//							}
//							
//						}
//					}
//				}
//			}
//
//		}
//		
//	}
//	
//	  private void fixDelete(TreeNode x) {
//		    TreeNode s;
//		    while (x != _root && x.color == BLACK && x != _nil) {
//		      if (x == x.p.left) {
//		        s = x.p.right;
//		        if (s.color == RED) {
//		          s.color = BLACK;
//		          x.p.color = RED;
//		          rotateLeft(x.p);
//		          s = x.p.right;
//		        }
//
//		        if (s.left.color == BLACK && s.right.color == RED) {
//		          s.color = RED;
//		          x = x.p;
//		        } else {
//		          if (s.right.color == BLACK) {
//		            s.left.color = BLACK;
//		            s.color = RED;
//		            rotateRight(s);
//		            s = x.p.right;
//		          }
//
//		          s.color = x.p.color;
//		          x.p.color = BLACK;
//		          s.right.color = BLACK;
//		          rotateLeft(x.p);
//		          x = _root;
//		        }
//		      } else {
//		        s = x.p.left;
//		        if (s.color == RED) {
//		          s.color = BLACK;
//		          x.p.color = RED;
//		          rotateRight(x.p);
//		          s = x.p.left;
//		        }
//
//		        if (s.right.color == BLACK && s.right.color == RED) {
//		          s.color = RED;
//		          x = x.p;
//		        } else {
//		          if (s.left.color == BLACK) {
//		            s.right.color = BLACK;
//		            s.color = RED;
//		            rotateLeft(s);
//		            s = x.p.left;
//		          }
//
//		          s.color = x.p.color;
//		          x.p.color = BLACK;
//		          s.left.color = BLACK;
//		          rotateRight(x.p);
//		          x = _root;
//		        }
//		      }
//		    }
//		    x.color = BLACK;
//		  }

}
